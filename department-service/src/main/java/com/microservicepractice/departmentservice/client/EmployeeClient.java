package com.microservicepractice.departmentservice.client;

import com.microservicepractice.departmentservice.entity.Employee;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;

import java.util.List;

/**
 * @author Ayonij Karki on 6/17/2023
 */
@HttpExchange
public interface EmployeeClient {
    @GetExchange("/employee/department/{id}")
    List<Employee> findByDepartmentId(@PathVariable("id") Long departmentId);

}
