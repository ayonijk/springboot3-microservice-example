package com.microservicepractice.employeeservice.entity;

/**
 * @author Ayonij Karki on 6/17/2023
 */
public record Employee(Long id, Long departmentId, String name, int age, String position) {
}
